<?php
// $Id$

/**
 * @file
 * Admin functions for fblikebutton.
 */

/**
 * Configure which node types can be "liked" by users.
 */
function fblikebox_admin_settings() {
	
  $form['fblikebox_fb_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Face book Page URL'),
    '#default_value' => variable_get('fblikebox_fb_page', ''),
    '#size' => 60,
    '#maxlengh' => 255,
    '#description' => t('The URL of the Facebook Page for this Like box.Block will be created only when the facebook page is provided'),
  );
  $form['fblikebox_show_faces'] = array(
    '#type' => 'radios',
    '#title' => t('Show faces in the Like box'),
    '#default_value' => variable_get('fblikebox_show_faces', 'true'),
    '#options' => array('true' => t('Show faces'), 'false' => t('Do not show faces')),
    '#description' => t('Should users see the faces of other people who have "liked" the same content?'),
  );
  $form['fblikebox_show_stream'] = array(
    '#type' => 'radios',
    '#title' => t('Show stream in the Like box'),
    '#default_value' => variable_get('fblikebox_show_stream', 'false'),
    '#options' => array('true' => t('Show stream'), 'false' => t('Do not show stream')),
    '#description' => t('Show the profile stream for the public profile '),
  );
  $form['fblikebox_show_header'] = array(
    '#type' => 'radios',
    '#title' => t('Show header in the Like box'),
    '#default_value' => variable_get('fblikebox_show_header', 'true'),
    '#options' => array('true' => t('Show header'), 'false' => t('Do not show header')),
    '#description' => t('Show the \'Find us on Facebook\' bar at top. Only shown when either stream or faces are present. '),
  );
   $form['fblikebox_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => variable_get('fblikebox_width', 300),
    '#size' => 60,
    '#maxlengh' => 255,
    '#description' => t('The width of the plugin in pixels.'),
  );
  $form['fblikebox_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => variable_get('fblikebox_height', 62),
    '#size' => 60,
    '#maxlengh' => 255,
    '#description' => t('The Height of the plugin in pixels.'),
  );
  
  return system_settings_form($form);
}
